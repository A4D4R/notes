# Slick JS
Quick how to use on slick js


## INCLUDE THIS ON BOTTOM:

### Library
```html
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="https://kenwheeler.github.io/slick/slick/slick-theme.css"/>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
```

### Javascript:
```js
$('.slider').slick({
    dots: true,
    infinite: true,
    speed: 600,
    slidesToShow: 1,    // change this
    adaptiveHeight: true,
    autoplay: true,
    autoplaySpeed: 5000,
});
```
---------------------

### SLIDER CODE:
```html
<div class="slider">
    <div>
        <img>
    </div>

    <div>
        ...
    </div>


    <div>
        ..
    </div>
</div>
```
